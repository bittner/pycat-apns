"""
@author (C) 2013 Gerardo Ezquerra (catwashere@yahoo.com)
@file sample_push.py
@version 0.1
"""
from __future__ import print_function

import sys

sys.path.insert(0, 'library')

from push import APNS_Push, APNS_Message

push = APNS_Push(APNS_Push.ENVIRONMENT_PRODUCTION, "certificate-prod.pem")

push.setRootCertificationAuthority("entrust_root_certification_authority.pem")

push.connect()

message = APNS_Message('b4b6b1999e71bb4283494bd138945c5a8a1cbaf75e6aa6add1655756bf56371c')
message.setText("TMP Text")
message.setBadge(1)
message.setSound()

push.add(message)

push.send()

errors = push.getErrors()
if len(errors) > 0:
    print(errors)

push.disconnect()
